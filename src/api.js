import { error } from "util";

const raw = (path, data, { method = "post", headers = {}, root }) => {
  return fetch(
    root ? `${root}${path}` : `${process.env.VUE_APP_API_URL}api/${path}`,
    {
      method,
      headers,
      body: data
    }
  ).then(function(response) {
    if (response.status == 200) {
      return response.json();
    } else {
       throw error;
    }
  });
};

const jsonWithAuth = (
  path,
  data,
  {
    method = "post",
    headers = { "Content-Type": "application/json" },
    root
  } = {}
) =>
  raw(path, JSON.stringify(data), {
    method,
    headers: {
      ...headers,
      Authorization: `Bearer ${sessionStorage.getItem("user-token")}`
    },
    root
  });

const json = (
  path,
  data,
  {
    method = "post",
    headers = { "Content-Type": "application/json" },
    root
  } = {}
) =>
  raw(path, JSON.stringify(data), {
    headers,
    method,
    root
  });

export default { jsonWithAuth, json, raw };
